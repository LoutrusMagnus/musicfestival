package com.project.musiquefestival3.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.musiquefestival3.services.ProgrammationService;
import com.project.musiquefestival3.entities.Programmation;

@RestController
@RequestMapping("programmation")
public class ProgrammationController {

	ProgrammationService pgService;
	
	public ProgrammationController(ProgrammationService pgService) {
		super();
		this.pgService = pgService;
	}
	
	@GetMapping("programmations")
	public List<Programmation> findAll() {
		return this.pgService.findAll();
	}
	
	@GetMapping("/programmations/{id}")
	public Optional<Programmation> findById(@PathVariable String id) {
		return this.pgService.findById(id);
	}
	
	@PostMapping("/programmation")
	public Programmation save(@RequestBody Programmation prog) {
		return this.pgService.save(prog);
	}
	
	@PutMapping("/programmation")
	public Programmation update(@RequestBody Programmation prog) {
		return this.pgService.save(prog);
	}
	
	@DeleteMapping("/programmation")
	public void delete(@RequestBody Programmation prog) {
		this.pgService.delete(prog);
	}
	
}
