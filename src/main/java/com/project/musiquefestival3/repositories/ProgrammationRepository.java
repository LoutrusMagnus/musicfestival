package com.project.musiquefestival3.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.project.musiquefestival3.entities.Programmation;

public interface ProgrammationRepository extends MongoRepository<Programmation, String>{

}
