package com.project.musiquefestival3.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.project.musiquefestival3.repositories.ProgrammationRepository;
import com.project.musiquefestival3.entities.Programmation;

@Service
public class ProgrammationService {

	private ProgrammationRepository pgRepo;
	
	public ProgrammationService( ProgrammationRepository pgRepo) {
		this.pgRepo = pgRepo;
	}
	
	public List<Programmation> findAll() {
		return this.pgRepo.findAll();
	}
	
	public Optional<Programmation> findById(String id) {
		return this.pgRepo.findById(id);
	}
	
	public Programmation save(Programmation prog) {
		return this.pgRepo.save(prog);
	}
	
	public void delete(Programmation prog) {
		this.pgRepo.delete(prog);
	}
}
